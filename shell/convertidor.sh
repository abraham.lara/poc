#!/bin/sh
#
# Convertir un archivo de texto en un archivo en formato xls
#

#Aquí convertimos el archivo en .txt a formato .xls
awk 'BEGIN{
        FS="|"
#        printf("<html><meta http-equiv=\"content-type\" content=\"application/vnd.ms-excel\"/>");
printf("<html><meta http-equiv=\"content-type\" content=\"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet\"/>");
       print "<body><table border=1>"
     }
     {
        printf "<tr>"
        for(i=1;i<=NF;i++)
          printf "<td>%s</td>", $i
        print "</tr>"
     }
     END{
        print "</table></body></html>"
     }
     ' ListServ.json > archivo_excel.xls

#
# Fin del sript
#
